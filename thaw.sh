#this command assumes it's in the ./tools directory off the WP root
#also it assumed a _data/ dir off root w/ a wordpress.sql file
#use this on a new c9 PHP workspace.
#
#
#start the database
cd ~/$C9_PID
mysql-ctl start
#import the .sql file
cd ~/$C9_PID/_data
mysql --user=$C9_USER --host=$IP c9 < wordpress.sql

#run find and replace on the .sql file
#grab the old project name from save file
read -r oldname<~/$C9_PID/_data/data.txt
cd ~/$C9_PID/_tools
#run the find and replace
./srdb.php --host $IP --user $C9_USER --database c9 --pass "" --charset utf\-8 --search "${oldname}" --replace "$C9_PROJECT"